# -*- coding: utf-8 -*-
from IPy import IP
import netifaces


def get_ips():
    '根据本机网卡信息，获取所有ip地址。'
    ipinfos=[]
    devs = netifaces.interfaces()
    for item in devs:
        if 'lo' not in item and 'Vir' not in item and 'vir' not in item and 'docker' not in item:
            ipinfo = netifaces.ifaddresses(item)
            if 2 in ipinfo:
                ipinfos.extend(ipinfo[2])
    return ipinfos


def get_net(addr,netmask):
    '''根据ip地址+子网掩码，获取ip网段'''
    return IP(addr).make_net(netmask)


def get_route():
    "获取路由信息"
    # def_gateways = netifaces.gateways()
    # def_gateways = raw_input('please input gateways:')
    def_gateways = '192.168.2.1'
    return def_gateways


if __name__ == '__main__':
    get_route()
